package com.masglobal.masglobaltest.model;

/**
 * This class has the business logic to create the implementations of Employee
 * interface.
 * 
 * @author Marcela Chaparro
 */
public class EmployeeFactory {

	/**
	 * This method returns the implementation of Employee class according to the
	 * given parameter contractType.
	 * 
	 * @param contractType: contractType that define the class type to return.
	 * @return Employee: Created object based on the provided parameter.
	 */
	public static Employee createEmployee(String contractType) {
		if (contractType == null) {
			return null;
		}
		switch (contractType) {
		case "HourlySalaryEmployee":
			return new EmployeeHourlySalary();
		case "MonthlySalaryEmployee":
			return new EmployeeMonthlySalary();
		default:
			return null;
		}
	}
}
