package com.masglobal.masglobaltest.model;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * This class implements the Employee class and its methods.
 * 
 * @author Marcela Chaparro
 */
public class EmployeeHourlySalary implements Employee {

	private String id;
	private String name;
	private String contractTypeName;
	private String roleId;
	private String roleName;
	private String roleDescription;
	private String hourlySalary;
	private String monthlySalary;
	private String annualSalary;

	/**
	 * Empty constructor
	 */
	public EmployeeHourlySalary() {
	}

	/**
	 * This method returns the employee identifier.
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method set the employee identifier.
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * This method returns the employee name.
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method set the employee name.
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * This method returns the employee contractTypeName.
	 */
	public String getContractTypeName() {
		return contractTypeName;
	}

	/**
	 * This method set the employee contractTypeName.
	 */
	public void setContractTypeName(String contractTypeName) {
		this.contractTypeName = contractTypeName;
	}

	/**
	 * This method returns the employee roleId.
	 */
	public String getRoleId() {
		return roleId;
	}

	/**
	 * This method set the employee roleId.
	 */
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
	 * This method returns the employee roleName.
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * This method set the employee roleName.
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * This method returns the employee roleDescription.
	 */
	public String getRoleDescription() {
		return roleDescription;
	}

	/**
	 * This method set the employee roleDescription.
	 */
	public void setRoleDescription(String roleDescription) {
		this.roleDescription = roleDescription;
	}

	/**
	 * This method returns the employee hourlySalary.
	 */
	public String getHourlySalary() {
		return hourlySalary;
	}

	/**
	 * This method set the employee hourlySalary.
	 */
	public void setHourlySalary(String hourlySalary) {
		this.hourlySalary = hourlySalary;
	}

	/**
	 * This method returns the employee monthlySalary.
	 */
	public String getMonthlySalary() {
		return monthlySalary;
	}

	/**
	 * This method set the employee monthlySalary.
	 */
	public void setMonthlySalary(String monthlySalary) {
		this.monthlySalary = monthlySalary;
	}

	/**
	 * This method returns the employee annualSalary.
	 */
	public String getAnnualSalary() {
		return annualSalary;
	}

	/**
	 * This method set the employee annualSalary.
	 */
	public void setAnnualSalary() {
		Double hourlySalary = Double.parseDouble(this.hourlySalary);
		Double annual = 120 * hourlySalary * 12;
		DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(Locale.US);
		decimalFormat.applyPattern("$#,##0.0");
		this.annualSalary = decimalFormat.format(Double.parseDouble(annual.toString()));
	}
}
