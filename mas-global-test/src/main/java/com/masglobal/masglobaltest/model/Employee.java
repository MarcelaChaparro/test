package com.masglobal.masglobaltest.model;

/**
 * This class is the interface.
 * 
 * @author Marcela Chaparro
 */
public interface Employee {

	String getId();

	void setId(String id);

	String getName();

	void setName(String name);

	String getContractTypeName();

	void setContractTypeName(String contractTypeName);

	String getHourlySalary();

	void setHourlySalary(String hourlySalary);

	String getMonthlySalary();

	void setMonthlySalary(String monthlySalary);

	String getRoleId();

	void setRoleId(String roleId);

	String getRoleName();

	void setRoleName(String roleName);

	String getRoleDescription();

	void setRoleDescription(String roleDescription);

	String getAnnualSalary();

	void setAnnualSalary();

}
