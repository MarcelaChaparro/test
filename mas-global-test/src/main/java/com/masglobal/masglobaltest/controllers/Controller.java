package com.masglobal.masglobaltest.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.masglobal.masglobaltest.model.Employee;
import com.masglobal.masglobaltest.model.EmployeeFactory;

/**
 * This class allows to manage the frontend requests.
 * 
 * @author Marcela Chaparro
 */
@RestController
@RequestMapping("/api")
public class Controller {

	final static String uri = "http://masglobaltestapi.azurewebsites.net/api/employees";

	/**
	 * This method allows to call the information including the calculated Annual
	 * Salary for a employee consulted from the UI.
	 * 
	 * @param employeeIdJson: String that contains the employee ID.
	 * @return ResponseEntity<Employee>: ResponseEntity with the employees'
	 *         information including the calculated Annual Salary
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@CrossOrigin
	@PostMapping("/employeesInfo")
	public ResponseEntity<Employee> getEmployeesInfo(@RequestBody String employeeIdJson) {
		try {
			String employeeId = JsonParser.parseString(employeeIdJson).getAsJsonObject().get("employeeId")
					.getAsString();
			List<Employee> employeesInfo = buildEmployeeInfo(employeeId);
			HttpStatus httpStatus = employeesInfo.size() > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND;
			return new ResponseEntity(employeesInfo, httpStatus);
		} catch (Exception e) {
			return new ResponseEntity("Failure", HttpStatus.BAD_REQUEST);
		}
	}

	/**
	 * This method allows to build the employee information according to the type of
	 * contract that given employee has
	 * 
	 * @param employeeSearched: Employeed ID searched in the interface
	 * @return List<Employee>: Employee list with the employee information typed by
	 *         the user or all employees in case the user has not searched for any.
	 * @throws Exception
	 */
	private static List<Employee> buildEmployeeInfo(String employeeSearched) throws Exception {

		List<Employee> employees = new ArrayList<Employee>();
		RestTemplate restTemplate = new RestTemplate();
		String jsonEmployees = restTemplate.getForObject(uri, String.class);
		JsonArray employeesArray = JsonParser.parseString(jsonEmployees).getAsJsonArray();
		for (int i = 0; i < employeesArray.size(); i++) {
			JsonElement element = employeesArray.get(i);
			String type = JsonParser.parseString(element.toString()).getAsJsonObject().get("contractTypeName")
					.getAsString();
			Employee employeeObj = EmployeeFactory.createEmployee(type);
			String id = parseJsonString(element, "id");
			String name = parseJsonString(element, "name");
			String roleId = parseJsonString(element, "roleId");
			String roleName = parseJsonString(element, "roleName");
			String roleDescription = parseJsonString(element, "roleDescription");
			String hourlySalary = parseJsonString(element, "hourlySalary");
			String monthlySalary = parseJsonString(element, "monthlySalary");
			employeeObj.setId(id);
			employeeObj.setName(name);
			employeeObj.setContractTypeName(type);
			employeeObj.setRoleId(roleId);
			employeeObj.setRoleName(roleName);
			employeeObj.setRoleDescription(roleDescription);
			employeeObj.setHourlySalary(hourlySalary);
			employeeObj.setMonthlySalary(monthlySalary);
			employeeObj.setAnnualSalary();
			employees.add(employeeObj);
		}
		if (employeeSearched != null && !employeeSearched.equals("")) {
			employees = employees.stream().filter(e -> e.getId().equals(employeeSearched)).collect(Collectors.toList());
		}
		return employees;
	}

	/**
	 * This method allows to parse and return the json element to string or an empty
	 * string if it is null.
	 * 
	 * @param element: Json element.
	 * @param item:    Item of the json to parse as string.
	 * @return String: Element as string or an empty string if it is null.
	 */
	private static String parseJsonString(JsonElement element, String item) {
		return JsonParser.parseString(element.toString()).getAsJsonObject().get(item).isJsonNull() ? ""
				: JsonParser.parseString(element.toString()).getAsJsonObject().get(item).getAsString();

	}
}
