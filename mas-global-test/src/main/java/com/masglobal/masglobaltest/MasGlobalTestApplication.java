package com.masglobal.masglobaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MasGlobalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MasGlobalTestApplication.class, args);
	}

}
