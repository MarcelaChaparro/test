import {connect} from 'react-redux';
import TableEmployees from '../components/tableEmployees.js';

const mapStateToProps = state => {
    return { employees: state.employees, consulted: state.consulted }
}

const mapDispatchToProps = dispatch => {
    return {
        resetEmployees: () =>  dispatch({type: 'RESET_EMPLOYEES'})
    }
}

const createConnection = connect(
    mapStateToProps, mapDispatchToProps
)

const connectionToRedux = createConnection(TableEmployees)

export default connectionToRedux