import {connect} from 'react-redux';
import EmployeeForm from '../components/employeeForm.js';
import axios from 'axios';

const mapStateToProps = state => {
    return { employeeId: state.employeeId, employees: state.employees}
}

const mapDispatchToProps = dispatch => {
    return {
        setEmployeeId: (val) => dispatch({type: 'SET_EMPLOYEE_ID', data:val}),
        setEmployees: (val) => {
            axios.post('http://localhost:8080/api/employeesInfo', {employeeId: val}).then((response) =>{
                dispatch({type: 'SET_EMPLOYEES', data: response.data})
            }).catch(e => {
                dispatch({type: 'SET_EMPLOYEES', data: []})
            })
            
        }
    }
}

const createConnection = connect(
    mapStateToProps,
    mapDispatchToProps
)

const connectionToRedux = createConnection(EmployeeForm)

export default connectionToRedux