
import { Provider } from 'react-redux';
import EmployeeApp from './reducers'
import { createStore } from 'redux';
import './App.css';
import EmployeeForm from './containers/employeeForm';
import TableEmloyees from './containers/tableEmployees';

function App() {
  const store = createStore(EmployeeApp);
  return (
    <div className="App d-flex">
      <div className="container">
        <Provider store={store}>
          <EmployeeForm />
          <TableEmloyees />
        </Provider>
      </div>
    </div>
  );
}

export default App;
