const INITIAL_STATE = {
  employees: [],
  employeeId: '',
  consulted: false,
};

const employeeState = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case 'SET_EMPLOYEE_ID':
      state.employeeId = action.data;      
      return  {...state};
    case 'RESET_EMPLOYEES':
      state.employees = [];
      state.consulted = false;
      return  {...state};
    case 'SET_EMPLOYEES':
      state.employees = action.data;
      state.consulted = true;
      return  {...state};
    default:
      return  {...state};
  }
};

export default employeeState;
