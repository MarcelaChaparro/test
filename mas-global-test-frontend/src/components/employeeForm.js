import React from 'react';
import { Button, Form, FormGroup, Label, Input, Row, Col } from 'reactstrap'

const EmployeeForm = (props) => {
  const setEmployees = () => {
    props.setEmployees(props.employeeId)
  }
  return (
    <div>
      <Row>
        <Col md={{ size: 4, offset: 4}}>
          <Form>
          <FormGroup>
            <Label for="exampleEmail">Employee ID:</Label>
            <Input
              value={props.employeeId}
              name="text"
              type="text"
              onChange={(event) => props.setEmployeeId(event.target.value)}
            />
          </FormGroup>
          <Button className="w-100" onClick={setEmployees} color="primary">Get Employees</Button>
        </Form>
        </Col>
      </Row>
      
    </div>
  );
};

export default EmployeeForm;
