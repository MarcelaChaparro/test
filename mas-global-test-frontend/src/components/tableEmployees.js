import React from 'react';
import { Modal, ModalHeader, ModalBody, Table } from 'reactstrap';
const numeral = require('numeral')

const TableEmployees = (props) => {
  
  const items = props.employees.map((item) => (
    <tr key={item.id}>
      <td>{item.name}</td>
      <td>{item.contractTypeName}</td>
      <td>{item.roleName}</td>
      <td>{item.roleDescription}</td>
      <td>{numeral(item.hourlySalary).format('$0,0[.]00')}</td>
      <td>{numeral(item.monthlySalary).format('$0,0[.]00')}</td>
      <td>{item.annualSalary}</td>
    </tr>
  ));

  const contentBody = () => {
    return items.length > 0 ? (
      <Table striped responsive>
        <thead>
          <tr>
            <th>Name</th>
            <th>Contract Type</th>
            <th>Role Name</th>
            <th>Role Description</th>
            <th>Hourly Salary</th>
            <th>Monthly Salary</th>
            <th>Annual Salary</th>
          </tr>
        </thead>
        <tbody>{items}</tbody>
      </Table>
    ) : (
      <p>There is no employee with the consulted ID</p>
    );
  };
  return (
    <div>
      <Modal isOpen={props.consulted} toggle={props.resetEmployees} size="xl">
        <ModalHeader toggle={props.resetEmployees}>
          Employees Information:
        </ModalHeader>
        <ModalBody>{contentBody()}</ModalBody>
      </Modal>
    </div>
  );
};

export default TableEmployees;
