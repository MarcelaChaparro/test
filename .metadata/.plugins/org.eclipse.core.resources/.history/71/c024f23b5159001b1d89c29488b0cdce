package com.masglobal.masglobaltest.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.masglobal.masglobaltest.model.Employee;
import com.masglobal.masglobaltest.model.EmployeeFactory;
import com.masglobal.masglobaltest.model.EmployeeHourlySalary;

@RestController
@RequestMapping("/api")
public class Controller {

	final static String uri = "http://masglobaltestapi.azurewebsites.net/api/employees";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@CrossOrigin
	@PostMapping("/employesInfo")
	// @GetMapping("/hello-world")
	// @RequestMapping(value = "/users", method = RequestMethod.GET)
	public ResponseEntity<EmployeeHourlySalary> get(@RequestBody String employeeIdJson) {
		try {
			String employeeId = JsonParser.parseString(employeeIdJson).getAsJsonObject().get("employeeId")
					.getAsString();
			List<Employee> employeesInfo = getEmployees(employeeId);
			HttpStatus httpStatus = employeesInfo.size() > 0 ? HttpStatus.OK : HttpStatus.NOT_FOUND;
			return new ResponseEntity(employeesInfo, httpStatus);
		} catch (Exception e) {
			return new ResponseEntity("Fallo", HttpStatus.BAD_REQUEST);
		}
	}

	private static List<Employee> getEmployees(String employeeSearched) throws Exception {

		List<Employee> employees = new ArrayList<Employee>();
		RestTemplate restTemplate = new RestTemplate();
		String jsonEmployees = restTemplate.getForObject(uri, String.class);
		// ObjectMapper mapper = new ObjectMapper();
		// List<Employee> employees = mapper.readValue(jsonEmployees,new
		// TypeReference<List<Employee>>() {});
		JsonArray employeesArray = JsonParser.parseString(jsonEmployees).getAsJsonArray();
		for (int i = 0; i < employeesArray.size(); i++) {
			JsonElement element = employeesArray.get(i);
			String type = JsonParser.parseString(element.toString()).getAsJsonObject().get("contractTypeName")
					.getAsString();
			Employee employeeObj = EmployeeFactory.createEmployee(type);
			String id = JsonParser.parseString(element.toString()).getAsJsonObject().get("id").getAsString();
			String name = JsonParser.parseString(element.toString()).getAsJsonObject().get("name").getAsString();
			String roleId = JsonParser.parseString(element.toString()).getAsJsonObject().get("roleId").getAsString();
			String roleName = JsonParser.parseString(element.toString()).getAsJsonObject().get("roleName")
					.getAsString();
			String roleDescription = element.isJsonNull() ? ""
					: JsonParser.parseString(element.toString()).getAsJsonObject().get("roleDescription").getAsString();
			String hourlySalary = JsonParser.parseString(element.toString()).getAsJsonObject().get("hourlySalary")
					.getAsString();
			String monthlySalary = JsonParser.parseString(element.toString()).getAsJsonObject().get("monthlySalary")
					.getAsString();
			employeeObj.setId(id);
			employeeObj.setName(name);
			employeeObj.setContractTypeName(type);
			employeeObj.setRoleId(roleId);
			employeeObj.setRoleName(roleName);
			employeeObj.setRoleDescription(roleDescription);
			employeeObj.setHourlySalary(hourlySalary);
			employeeObj.setMonthlySalary(monthlySalary);
			employeeObj.setAnnualSalary();
			employees.add(employeeObj);
		}

		if (employeeSearched != null && !employeeSearched.equals("")) {
			employees = employees.stream().filter(e -> e.getId().equals(employeeSearched)).collect(Collectors.toList());
		}
		return employees;
	}

	private static Double annualSalary(Employee employee) {
		Double salary = null;
		if (employee.getContractTypeName().equals("HourlySalaryEmployee")) {
			Double hourlySalary = Double.parseDouble(employee.getHourlySalary());
			salary = 120 * hourlySalary * 12;
		} else if (employee.getContractTypeName().equals("MonthlySalaryEmployee")) {
			Double monthtlySalary = Double.parseDouble(employee.getMonthlySalary());
			salary = monthtlySalary * 12;
		}

		return salary;
	}

}
