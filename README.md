** MAS Global test

This space in bitbucket contains the backend and frontend projects according to the specifications of Mas Global test.

Backend project: "mas-global-test" folder
Frontend project: "mas-global-test-frontend" folder

AUTHOR: MARCELA CHAPARRO

---

## Back-end project

The backend project / API was developed with Springboot using the Spring Tool Suite 4. It's a Java 8 project

---

## Front-end project

The Frontend project was developed with React. Using Redux and following the pattern container-component.
Using Node version 10.15.0 and React 17.0.1

---